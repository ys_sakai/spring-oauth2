package com.example.oauth2.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenRevocationController {

    private static final String HEADER_AUTHORIZATION = "Authorization";

    private static final String TOKEN_TYPE = "Bearer";

    @Autowired
    private ConsumerTokenServices consumerTokenService;

    @DeleteMapping("/oauth/revoke")
    @ResponseBody
    public void revokeToken(HttpServletRequest request) {
        String authorization = request.getHeader(HEADER_AUTHORIZATION);
        if (authorization != null && authorization.contains(TOKEN_TYPE)) {
            String token = authorization.substring(TOKEN_TYPE.length() + 1);
            consumerTokenService.revokeToken(token);
        }
    }
}
