package com.example.oauth2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.oauth2.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByName(String name);
}
