package com.example.oauth2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.oauth2.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
